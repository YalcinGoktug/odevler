/****************************************************************************
**							SAKARYA �N�VERS�TES�
**			         B�LG�SAYAR VE B�L���M B�L�MLER� FAK�LTES�
**				    	B�LG�SAYAR M�HEND�SL��� B�L�M�
**				          PROGRAMLAMAYA G�R��� DERS�
**	
**				�DEV NUMARASI�...:2.3
**				��RENC� ADI......:Ali G�ktu� Yal��n
**				��RENC� NUMARASI.:G1612.10101
**				DERS GRUBU.......:1A
****************************************************************************/

#include "stdafx.h"
#include <iostream>


using namespace std;

int main()
{
	setlocale(LC_ALL, "Turkish");
	int a, b, c, delta;
	cout << "ax2+bx+c �eklinde ki denklemin a,b ve c de�erlerini giriniz : "<<endl;
	cout << "a : ";
	cin >> a;
	cout << "b : ";
	cin >> b;
	cout << "c : ";
	cin >> c;
	delta = b*b-4*(a*c);
	if (delta < 0)//Diskriminant'�n prensiplerine g�re e�er delta 0'dan k���kse k�klerin karma��k oldu�unu ekrana basar.
			cout << "k�kler karma��kt�r..."<<endl;
	if (delta > 0)//Diskriminant'�n prensiplerine g�re 0'dan b�y�k delta durumunda k�kleri hesaplay�p ekrana basar.
	{
		cout << "k�kleri : " << endl;
		cout << "K�k 1 : " << ((b*-1) - ((delta ^ (1 / 2)))) / 2 * a << endl;
		cout << "K�k 2 : " << ((b*-1) + (delta ^ (1 / 2))) / 2 * a << endl;
	}
	if (delta == 0)//delta'n�n 0 olma durumunu kontrol eder ve k�k hesab�na ili�kin n�merik hesaplamalar yap�p ekrana basar.
	{
		cout << "tek k�kl�d�r : ";
		cout << ((b*-1) + (delta ^ (1 / 2))) / 2 * a<<endl;
	}
    return 0;
}

