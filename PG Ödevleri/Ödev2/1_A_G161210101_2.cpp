/****************************************************************************
**							SAKARYA ÜNİVERSİTESİ
**			         BİLGİSAYAR VE BİLİŞİM BİLİMLERİ FAKÜLTESİ
**				    	BİLGİSAYAR MÜHENDİSLİĞİ BÖLÜMÜ
**				          PROGRAMLAMAYA GİRİŞİ DERSİ
**	
**				ÖDEV NUMARASI....:2.2
**				ÖĞRENCİ ADI......:Ali Göktuğ Yalçın
**				ÖĞRENCİ NUMARASI.:G1612.10101
**				DERS GRUBU.......:1A
****************************************************************************/
//#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Turkish");
	int boyutDizi = 0;
	cout << "Dizi boyutu giriniz : ";
	cin >> boyutDizi;
	int* dizi = new int[boyutDizi];
	srand((int)time(0));


	for (int sira = 0; sira < boyutDizi; sira++)//Diziye random eleman atama
	{
		dizi[sira] = rand() % 9 + 1;
	}
	system("cls");
	int index = 0;
	char yon = '\NULL';
	while(true)//Sürekli çalışan bir döngü sağlar.
	{ 
		if (index < 0)//index'i kontrol edip ayarlar.
			index = 0;
		if (index >= boyutDizi)//index'i kontrol edip ayarlar.
			index = boyutDizi - 1;
	for (int sira = 0; sira < boyutDizi; sira++)//sira ve index değişkenlerine bağlı olarak ekrana boşluk veya ok yazar. 
	{
		if (sira == index)//Ekrana ok yazar.
			cout << "-->";
		else//Ekrana ok'un kapladığı yer kadar boşluk yazar.
			cout << "   ";
		cout << dizi[sira]<<endl;
	}
	
	cout << "Yön giriniz(a aşağı, d yukarı gider; c programdan çıkış yapar) : ";
	cin >> yon;
	
	if (yon == 'a' || yon == 'A')//index'i kontrol edip yön belirler.
		index++;
	if (yon == 'd' || yon == 'D')//index'i kontrol edip yön belirler.
		index--;
	if (yon == 'c' || yon == 'C')//index'i kontrol edip programdan çıkış yapar.
		break;
	
	system("cls");
	}
    return 0;
}

