/****************************************************************************
**							SAKARYA ÜNİVERSİTESİ
**			         BİLGİSAYAR VE BİLİŞİM BİLİMLERİ FAKÜLTESİ
**				    	BİLGİSAYAR MÜHENDİSLİĞİ BÖLÜMÜ
**				          PROGRAMLAMAYA GİRİŞİ DERSİ
**	
**				ÖDEV NUMARASI…...:2.1
**				ÖĞRENCİ ADI......:Ali Göktuğ Yalçın
**				ÖĞRENCİ NUMARASI.:G1612.10101
**				DERS GRUBU.......:1A
****************************************************************************/


#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Turkish");
	float notVize, notOdev1, notOdev2, notQuiz1, notQuiz2, notFinal;
	double yuzdeVize, yuzdeOdev, yuzdeQuiz, yuzdeYilIci;

	cout << "Vize notunu giriniz : ";
	cin >> notVize;
	cout << endl;

	cout << "1. Ödev notunu giriniz : ";
	cin >> notOdev1;
	cout << endl;

	cout << "2. Ödev notunu giriniz : ";
	cin >> notOdev2;
	cout << endl;

	cout << "1. Kısa Sınav notunu giriniz : ";
	cin >> notQuiz1;
	cout << endl;

	cout << "2. Kısa Sınav notunu giriniz : ";
	cin >> notQuiz2;
	cout << endl;

	cout << "Final Sınavı notunu giriniz : ";
	cin >> notFinal;
	cout << endl;

	cout << "Vize'nin yıl içi etki yüzdesini giriniz : ";
	cin >> yuzdeVize;
	cout << endl;

	cout << "Ödevlerin yıl içi etki yüzdesini giriniz : ";
	cin >> yuzdeOdev;
	cout << endl;

	cout << "Kısa Sınavların yıl içi etki yüzdesini giriniz : ";
	cin >> yuzdeQuiz;
	cout << endl;

	cout << "Yıl içi puanın etkisi yüzdesini giriniz : ";
	cin >> yuzdeYilIci;

	cout << endl << "-------------------------------------------------------------------------" << endl;
	double toplamYilIci = ((notVize * yuzdeVize) / 100) + ((notOdev1*yuzdeOdev) / 100) + ((notOdev2*yuzdeOdev) / 100) + ((notQuiz1*yuzdeQuiz) / 100) + ((notQuiz2*yuzdeQuiz) / 100);
	cout <<endl<< "Yıl içi toplam yüzde : " << toplamYilIci <<" / "<< yuzdeYilIci<<endl<<endl;
	double toplamFinal = ((notFinal*(100 - yuzdeYilIci)) / 100);
	

	cout << "Yıl Sonu puanınız : " << toplamFinal + toplamYilIci;
	if (toplamFinal + toplamYilIci < 49 && toplamFinal + toplamYilIci>0)//0-49 Not aralığı kontrol ediliyor.
		cout << " FF" << endl;
	if (toplamFinal + toplamYilIci < 57 && toplamFinal + toplamYilIci>50)//57-50 Not aralığı kontrol ediliyor.
		cout << " DD" << endl;
	if (toplamFinal + toplamYilIci < 64 && toplamFinal + toplamYilIci>58)//64-58 Not aralığı kontrol ediliyor.
		cout << " DC" << endl;
	if (toplamFinal + toplamYilIci < 74 && toplamFinal + toplamYilIci>65)//74-65 Not aralığı kontrol ediliyor.
		cout << " CC" << endl;
	if (toplamFinal + toplamYilIci < 79 && toplamFinal + toplamYilIci>75)//79-75 Not aralığı kontrol ediliyor.
		cout << " CB" << endl;
	if (toplamFinal + toplamYilIci < 84 && toplamFinal + toplamYilIci>80)//84-80 Not aralığı kontrol ediliyor.
		cout << " BB" << endl;
	if (toplamFinal + toplamYilIci < 89 && toplamFinal + toplamYilIci>85)//89-85 Not aralığı kontrol ediliyor.
		cout << " BA" << endl;
	if (toplamFinal + toplamYilIci < 100 && toplamFinal + toplamYilIci>90)//100-90 Not aralığı kontrol ediliyor.
		cout << " AA" <<endl;

	return 0;
}

