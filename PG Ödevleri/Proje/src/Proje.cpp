

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
#include <conio.h>
#include <cstdio>
#include <iomanip>

using namespace std;

class Oda
{
public:
	int oda_Numara;
	int oda_Ucret;
	int musteri_Numara;
	string musteri_Ad;
	string musteri_Soyad;
	string musteri_Kimlik;

	void BilgiGir()
	{
		cout << "Oda Kayıt İşlemleri" << endl;
		cout << "-------------------" << endl;
		cout << "Müşterinin numarasını giriniz : ";
		cin >> musteri_Numara;
		cout << "Oda ücreti giriniz : ";
		cin >> oda_Ucret;
		cout << "Müşteri adı giriniz : ";
		cin >> musteri_Ad;
		cout << "Müşteri soyadı giriniz : ";
		cin >> musteri_Soyad;
		cout << "Müşterinin kimlik numarasını giriniz : ";
		cin >> musteri_Kimlik;
		cout << "-------------------" << endl;
		oda_Numara = musteri_Numara;
	}
	void Yazdir()
	{
		cout.setf(ios::left);
		cout << "--------------------------"<<endl;
		cout << "Müşterinin oda numarası : " << musteri_Numara << endl;
		cout << "Müşterinin adı : " << musteri_Ad << endl;
		cout << "Müşterinin Soyadı : " << musteri_Soyad << endl;
		cout << "Odanın ücreti : " << oda_Ucret << endl;
		cout << "Müşterinin Kimlik Numarası : " << musteri_Kimlik << endl;
		cout << "Müşteri numarası : " << musteri_Numara << endl;
		
	}
	void Sil(int odaNumara)
	{
		fstream dosya;
		fstream gecici;
		dosya.open("Veriler.txt",ios::in||ios::out);
		gecici.open("Gecici.txt", ios::in || ios::out);
		while (!dosya.eof())
		{

		}
		
		dosya.close();
		
	}
};

void AnaMenu(int menu)
{
	if (menu == 0)
	{
		cout << "Otel İşlemleri" << endl;
		cout << "-------------------" << endl;
		cout << "1-Oda İşlemleri" << endl << "2-Müşteri İşlemleri" << endl;
		cout << "3-Oda Kayıt İşlemleri" << endl << "4-Cıkış" << endl;
		cout << "Seçiminiz : ";
	}
	if (menu==1)
	{
		cout << "Oda İşlemleri" << endl;
		cout << "-------------------" << endl;
		cout << "1-Oda Ekle" << endl;
		cout << "2-Oda Sil"<<endl;
		cout << "3-Odaları Listele" << endl;
		cout << "4-Üst Menüye Dön" << endl;
		cout << "Seçiminiz : ";
	}
}
int main()
{
	fstream veri;
	int secim, secim2;
	setlocale(LC_ALL, "Turkish");
	while (true)
	{
		Oda oda;
		AnaMenu(0);
		cin >> secim;
		while (secim > 4 || secim < 1)
		{
			cout << "Hatalı seçim yaptınız, Devam Etmek için bir tuşa basınız...";
			_getch();
			system("cls");
			AnaMenu(0);
			cin >> secim;
		}
		if (secim == 1)
		{
			system("cls");
			AnaMenu(1);
			cin >> secim2;
			while (secim > 4 || secim < 1)//Seçim kontrolü
			{
				cout << "Hatalı seçim yaptınız, Devam Etmek için bir tuşa basınız...";
				_getch();
				system("cls");
				AnaMenu(1);
				cin >> secim2;
			}
			if (secim2 == 1)
			{

			}
			if (secim2 == 2)//Oda silme
			{

				int sil;
				cout << "Silmek istediğiniz oda numarası : ";
				cin >> sil;
				oda.Sil(sil);
				fstream veriler,gecici;
				veriler.open("Veriler.txt", ios::out || ios::in || ios::app);
				gecici.open("Gecici.txt", ios::out || ios::in || ios::app);
				while (!veriler.eof())
				{
					veriler >> oda.oda_Numara;
					veriler >> oda.musteri_Ad;
					veriler >> oda.musteri_Soyad;
					veriler >> oda.oda_Ucret;
					veriler >> oda.musteri_Kimlik;
					veriler >> oda.musteri_Numara;
					if (oda.oda_Numara == sil)
						continue;
					else
					{
						gecici << setw(20) << oda.oda_Numara
							<< setw(20) << oda.musteri_Ad
							<< setw(20) << oda.musteri_Soyad
							<< setw(20) << oda.oda_Ucret
							<< setw(20) << oda.musteri_Kimlik
							<< setw(20) << oda.musteri_Numara
							<< endl;
					}
			}
				remove("Veriler.txt");
				rename("Gecici.txt", "Veriler.txt");
				gecici.close();
				
			}
			if (secim2 == 3)//Odaları Yazdırma
			{
				ifstream veriler;
				Oda exc;
				veriler.open("Veriler.txt", ios::out || ios::in || ios::app);
				while (!veriler.eof())
				{
					veriler >> oda.oda_Numara;
					veriler >> oda.musteri_Ad;
					veriler >> oda.musteri_Soyad;
					veriler >> oda.oda_Ucret;
					veriler >> oda.musteri_Kimlik;
					veriler >> oda.musteri_Numara;
					if (exc.oda_Numara == oda.oda_Numara)
						break;
					oda.Yazdir();
					exc.oda_Numara=oda.oda_Numara;
					exc.musteri_Ad=oda.musteri_Ad;
					exc.musteri_Soyad=oda.musteri_Soyad;
					exc.oda_Ucret=oda.oda_Ucret;
					exc.musteri_Kimlik=oda.musteri_Kimlik;
					exc.musteri_Numara=oda.musteri_Numara;
				}
				veriler.close();
				_getch();
			}
			if (secim2 == 4)//Üst menüye çıkış
			{
				system("cls");
			}
		}
		if (secim == 3)//Yeni müşteri kaydı
		{
			veri.open("Veriler.txt", ios::app);
			veri.setf(ios::left);
			system("cls");
			
			oda.BilgiGir();
			veri << setw(20) << oda.oda_Numara
				<< setw(20) << oda.musteri_Ad
				<< setw(20) << oda.musteri_Soyad
				<< setw(20) << oda.oda_Ucret
				<< setw(20) << oda.musteri_Kimlik
				<< setw(20) << oda.musteri_Numara
				<< endl;
			veri.close();
		}
		if (secim == 4)//Programdan çıkış
		{
			return 0;
		}
		system("cls");
		
	}
}

