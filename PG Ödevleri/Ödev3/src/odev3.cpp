#include <iostream>
#include <string.h>
#include <conio.h>

using namespace std;

struct cumle
{
    char dizi[250];

    void KelimeSayisi()
    {
        int sira = 0, sayiKelime = 1;
        while (dizi[sira] != '\0')
        {
            if (dizi[sira] == ' ')
                sayiKelime++;
            sira++;
        }
        cout << "Kelime sayisi : " << sayiKelime << endl;
    }
    void HarfCumle()
    {
        int sira = 0, sayiHarf = 0;
        while (dizi[sira] != '\0')
        {
            if (dizi[sira] == ' ' || dizi[sira] == '.' || dizi[sira] == '?' || dizi[sira] == '!' || dizi[sira] == ',')
                sira = sira;
            else
                sayiHarf++;
            sira++;
        }
        cout << "Cumlede ki harf sayisi : " << sayiHarf << endl;
    }
    void NoktalamaCumle()
    {
        int sira = 0, sayiNoktalama = 0;
        while (dizi[sira] != '\0')
        {
            if (dizi[sira] == '.' || dizi[sira] == '?' || dizi[sira] == '!' || dizi[sira] == ',')
                sayiNoktalama++;
            else
                sira = sira;
            sira++;
        }
        cout << "Cumlede ki noktalama sayisi : " << sayiNoktalama << endl;
    }
    void KelimeHarf()
    {
        int sira = 0, siraKelime = 1, sayiHarf = 0;
        while (dizi[sira] != '\0')
        {
            sayiHarf++;
            if(dizi[sira]==','||dizi[sira]=='.'||dizi[sira]=='!'||dizi[sira]=='?'||dizi[sira]==' ')
            {
                if(dizi[sira]==' ')
                sayiHarf--;
                if(dizi[sira]==' '&&(dizi[sira+1]==','||dizi[sira+1]=='.'||dizi[sira+1]=='!'||dizi[sira+1]=='?'))
                sira++;
                cout << siraKelime << ". Kelimenin harf sayisi : " << sayiHarf << endl;
                sayiHarf=0;
                siraKelime++;
            }

            sira++;
        }
        cout << siraKelime << ". Kelimenin harf sayisi : " << sayiHarf << endl;
    }
    bool PalindromKontrol()
    {
        char ters[250] = "";                     /* modified 1 */

        for (int i=0; i<strlen(dizi); i++)         /* modified 2 */
        {
            ters[i] = dizi[strlen(dizi)-i-1];       /* modified 2 */
            //q++;                                /* modified 3 */
        }
        for (int i=0; i<strlen(dizi); i++)         /* modified 2 */
        {
            if (dizi[i] != ters[i])
                return 0;
        }
        return 1;
    }
    void SesliHarf()
    {
        int sira = 0, sayi = 0;
        char harfler[7] = {'a', 'e', 'i', 'o', 'u', '\0'}; //c++'ın kodlaması ı,ö,ü harflerini kabul etmemekte.
        while (dizi[sira] != '\0')
        {
            for (int kontrol = 0; kontrol < 7; kontrol++)
            {
                if (dizi[sira] == harfler[kontrol])
                    sayi++;
            }
            sira++;
        }
        cout << "Cumlede ki sesli harf sayisi : " << sayi;
    }
};

int main()
{
    setlocale(LC_ALL, "Turkish");
    struct cumle deneme;
    cin.getline(deneme.dizi, sizeof(deneme.dizi));
    deneme.KelimeSayisi();
    deneme.HarfCumle();
    deneme.NoktalamaCumle();
    deneme.KelimeHarf();
    if(deneme.PalindromKontrol())
    {
        cout<<"Bu bir Palindromdur"<<endl;
    }
    else
    cout<<"Palindromluk bulunamadi..."<<endl;
    deneme.SesliHarf();
}